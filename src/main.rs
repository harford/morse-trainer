#![no_std]
#![no_main]
#![feature(abi_avr_interrupt)]

// Import necessary crates and modules
use panic_halt as _;
use avr_device::atmega32u4::TC1;
use avr_device::atmega32u4::tc1::tccr1b::CS1_A;
use avr_device::atmega32u4::TC3;
use avr_device::atmega32u4::tc3::tccr3b::CS3_A;
use core::sync::atomic::{AtomicBool, Ordering};

static TIMER_TICK: AtomicBool = AtomicBool::new(false);

pub const fn calc_overflow(clock_hz: u32, target_hz: u32, prescale: u32) -> u32 {
    /*
    https://github.com/Rahix/avr-hal/issues/75
    reversing the formula F = 16 MHz / (256 * (1 + 15624)) = 4 Hz
     */
    clock_hz / target_hz / prescale - 1
}

pub fn enable_timer(tmr1: &TC1) {
    tmr1.timsk1.write(|w| w.ocie1a().set_bit()); //enable this specific interrupt
}

pub fn disable_timer(tmr1: &TC1) {
    tmr1.timsk1.write(|w| w.ocie1a().clear_bit());
}

pub fn setup_timer_1(tmr1: &TC1) {
    /*
     https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7810-Automotive-Microcontrollers-ATmega328P_Datasheet.pdf
     section 15.11
    */
    use arduino_hal::clock::Clock;

    const ARDUINO_UNO_CLOCK_FREQUENCY_HZ: u32 = arduino_hal::DefaultClock::FREQ;
    const CLOCK_SOURCE: CS1_A = CS1_A::PRESCALE_256;
    let clock_divisor: u32 = match CLOCK_SOURCE {
        CS1_A::DIRECT => 1,
        CS1_A::PRESCALE_8 => 8,
        CS1_A::PRESCALE_64 => 64,
        CS1_A::PRESCALE_256 => 256,
        CS1_A::PRESCALE_1024 => 1024,
        // CS1_A::NO_CLOCK | CS1_A::EXT_FALLING | CS1_A::EXT_RISING => 0,
        CS1_A::NO_CLOCK | CS1_A::EXT_FALLING | CS1_A::EXT_RISING => {
            1
        }
    };

    let ticks = calc_overflow(ARDUINO_UNO_CLOCK_FREQUENCY_HZ, 1, clock_divisor) as u16;

    tmr1.tccr1a.write(|w| w.wgm1().bits(0b00));
    tmr1.tccr1b.write(|w| {
        w.cs1()
            //.prescale_256()
            .variant(CLOCK_SOURCE)
            .wgm1()
            .bits(0b01)
    });
    tmr1.ocr1a.write(|w| w.bits(ticks));
}

pub fn setup_timer_3(tmr3: &TC3) {
    /*
     https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7810-Automotive-Microcontrollers-ATmega328P_Datasheet.pdf
     section 15.11
    */
    use arduino_hal::clock::Clock;

    const ARDUINO_UNO_CLOCK_FREQUENCY_HZ: u32 = arduino_hal::DefaultClock::FREQ;
    const CLOCK_SOURCE: CS3_A = CS3_A::PRESCALE_256;
    let clock_divisor: u32 = match CLOCK_SOURCE {
        CS3_A::DIRECT => 1,
        CS3_A::PRESCALE_8 => 8,
        CS3_A::PRESCALE_64 => 64,
        CS3_A::PRESCALE_256 => 256,
        CS3_A::PRESCALE_1024 => 1024,
        CS3_A::NO_CLOCK | CS3_A::EXT_FALLING | CS3_A::EXT_RISING => {
            1
        }
    };

    let ticks = calc_overflow(ARDUINO_UNO_CLOCK_FREQUENCY_HZ, 600, clock_divisor) as u16;

    // Setup as PWM output
    tmr3.tccr3a.write(|w| w.com3a().bits(0b01)); 
    tmr3.tccr3a.write(|w| w.com3b().bits(0b00)); 
    tmr3.tccr3a.write(|w| w.wgm3().bits(0b00));
    tmr3.tccr3b.write(|w| {
        w.cs3()
            //.prescale_256()
            .variant(CLOCK_SOURCE)
            .wgm3()
            .bits(0b01)
    });
    tmr3.ocr3a.write(|w| w.bits(ticks));
}

#[avr_device::interrupt(atmega32u4)]
fn TIMER1_COMPA() {
    TIMER_TICK.store(true, Ordering::SeqCst);
}

#[arduino_hal::entry]
fn main() -> ! {

    // Get the peripherals
    let dp = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(dp);
    let mut speaker = pins.d5.into_floating_input();

    let tmr1: TC1 = dp.TC1;
    setup_timer_1(&tmr1);

    let tmr3: TC3 = dp.TC3;
    setup_timer_3(&tmr3);

    // Avoid flood of interrupts from USB controller
    dp.USB_DEVICE.usbcon.reset();

    // Enable global interrupts
    unsafe {
        avr_device::interrupt::enable();
    }

    enable_timer(&tmr1);

    loop {
        let current = TIMER_TICK.load(Ordering::SeqCst);
        if current {
            let mut spkout = speaker.into_output();

            spkout.set_high();

            speaker = spkout.into_floating_input();

            TIMER_TICK.store(false, Ordering::SeqCst);
        }
    }
}
